/*!
 * QUnit 2.4.1
 * https://qunitjs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2017-10-22T05:12Z
 */
(function (global$1) {
    'use strict';
  
    global$1 = 'default' in global$1 ? global$1['default'] : global$1;
  
    var window = global$1.window;
    var self$1 = global$1.self;
    var console = global$1.console;
    var setTimeout = global$1.setTimeout;
    var clearTimeout = global$1.clearTimeout;
  
    var document = window && window.document;
    var navigator = window && window.navigator;
  
    var localSessionStorage = function () {
        var x = "qunit-test-string";
        try {
            global$1.sessionStorage.setItem(x, x);
            global$1.sessionStorage.removeItem(x);
            return global$1.sessionStorage;
        } catch (e) {
            return undefined;
        }
    }();
  
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
      return typeof obj;
    } : function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  
  
  
  
    <script>var video = document.getElementById("display").firstElementChild;
    
    function toastOrder(){
        var toast5 = document.getElementById("1").checked;
        var toast10 = document.getElementById("2").checked;
        var toast50 = document.getElementById("3").checked;
        
        if(toast5 == true){
            document.getElementById("display").innerHTML = "Yaaaaayy!!!"; 
            
        }
        else if(toast10 == true){
            document.getElementById("display").innerHTML = "OOOOPs"; 
        }
        else{
            document.getElementById("display").innerHTML = "Whoa!!!";          
        }
    }</script>
  
  
  
  
  
  
    var classCallCheck = function (instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
      }
    };
  
    var createClass = function () {
      function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
          var descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ("value" in descriptor) descriptor.writable = true;
          Object.defineProperty(target, descriptor.key, descriptor);
        }
      }
  
      return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor;
      };
    }();