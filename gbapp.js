var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body

var app = express()  // make express app
var http = require('http').createServer(app)  // inject app into the server
var fs = require('fs')

var io = require('socket.io')(http)

// 1 set up the view engine
// 2 manage our entries
// 3 set up the logger
// 4 handle valid GET requests
// 5 handle valid POST request
// 6 respond with 404 if a bad URI is requested

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine
app.use(express.static(__dirname + '/assets'))



// Initialize app with route / (the root) "on getting a request to /, do the following"
// 2. replace the inside lines of your app.get 
//app.get('/', function (req, res) {
    //app.use(express.static(path.join(__dirname + '/views')))   
    //res.sendFile(path.join(__dirname, '../A03Katsion/views', 'index.html'))
//})

// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
    response.sendFile(__dirname+"/views/index.html")
})
app.get("/index.html", function (request, response) {
    response.sendFile(__dirname+"/views/index.html")
})
app.get("/Calculate.html", function (request, response) {
    response.sendFile(__dirname+"/views/Calculate.html")
})
app.get("/contact.html", function (request, response) {
    response.sendFile(__dirname+"/views/contact.html")
})

app.get("/views", function (request, response) {
    response.render("index")
  })
app.get("/new-entry", function (request, response) {
    response.render("new-entry")
  })

  // 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
    if (!request.body.title || !request.body.body) {
      response.status(400).send("Entries must have a title and a body.")
      return
    }
    entries.push({  // store it
      title: request.body.title,
      content: request.body.body,
      published: new Date()
    })
    response.redirect("/")  // where to go next? Let's go to the home page :)
  })
app.post("/contact", function (request, response) {
    var api_key = 'key-74debe84dfeecfec7d28d387b0a6fccf';
    var domain = 'sandboxc36fb167b02048188c3d38a5716e9c6f.mailgun.org';
    var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
     
    var data = {
      from: 'Mail Gun <postmaster@sandboxc36fb167b02048188c3d38a5716e9c6f.mailgun.org>',
      to: 's530242@nwmissouri.edu',
      subject: request.body.title,
      text: request.body.message
    }
     
    mailgun.messages().send(data, function (error, body) {
      console.log(body)
      if(!error)
        response.send("Mail sent!")
      else
        response.send("Mail not sent!")
    })
})
  // if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
    response.status(404).render("404")
  })
  
  // Listen for an application request on port 8081 & notify the developer
  http.listen(8081, function () {
    console.log('Guestbook app listening on http://127.0.0.1:8081/')
  })

  function toastOrder(){
    var toast5 = document.getElementById("1").checked;
    var toast10 = document.getElementById("2").checked;
    var toast50 = document.getElementById("3").checked;
    
    if(toast5 == true){
        document.getElementById("display").innerHTML = "Yaaaaayy!!!"; 
        
    }
    else if(toast10 == true){
        document.getElementById("display").innerHTML = "OOOOPs"; 
    }
    else{
        document.getElementById("display").innerHTML = "Whoa!!!";          
    }
}
  